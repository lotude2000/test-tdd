@extends('layouts.app')

@section('content')

    <div class="card">
        <h5 class="card-header">Task</h5>
        <div class="card-body">
            <form method="post" action="{{route('tasks.store')}}">
                @csrf
                <div class="form-group">
                    <label for="inputTitle" class="col-form-label">Name <span class="text-danger">*</span></label>
                    <input id="inputTitle" type="text" name="name" placeholder="Enter name"  value="{{old('name')}}" class="form-control">
                    @error('name')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="inputTitle" class="col-form-label">Content <span class="text-danger">*</span></label>
                    <input id="inputTitle" type="text" name="content" placeholder="Enter content"  value="{{old('content')}}" class="form-control">
                    @error('content')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group mb-3">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
