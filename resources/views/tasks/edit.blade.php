@extends('layouts.app')

@section('content')

    <div class="card">
        <h5 class="card-header">Task</h5>
        <div class="card-body">
            <form method="post" action="{{route('tasks.update',$task->id)}}">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="inputTitle" class="col-form-label">Name <span class="text-danger">*</span></label>
                    <input id="inputTitle" type="text" name="name" placeholder="Enter name"  value="{{$task->name}}" class="form-control">
                    @error('name')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="inputTitle" class="col-form-label">Content <span class="text-danger">*</span></label>
                    <input id="inputTitle" type="text" name="content" placeholder="Enter content"  value="{{$task->content}}" class="form-control">
                    @error('content')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group mb-3">
                    <button class="btn btn-success" type="submit">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
