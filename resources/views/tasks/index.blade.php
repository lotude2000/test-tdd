@extends('layouts.app')

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary float-left">Task List</h6>
            <a href="{{route('tasks.create')}}" class="btn btn-primary btn-sm float-right" data-toggle="tooltip" data-placement="bottom"
               title="Add User"><i class="fas fa-plus"></i> Add Task</a>
        </div>

    </div>
    <div class="card-body">
        <div class="table-responsive">
            @if(count($tasks)>0)
                <table class="table table-bordered" id="banner-dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{$task->id}}</td>
                            <td>{{$task->name}}</td>
                            <td>{{$task->content}}</td>
                            <td>
                                <a href="{{route('tasks.edit',$task->id)}}" class="btn btn-primary btn-sm float-left mr-1"
                                   style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit"
                                   data-placement="bottom"><i class="fas fa-edit"></i></a>
                                <a href="{{route('tasks.show',$task->id)}}" class="btn btn-primary btn-sm btn-success"
                                   style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="view"
                                   data-placement="bottom"><i class="fas fa-eye"></i></a>
                                <form method="POST" action="{{route('tasks.destroy',$task->id)}}}">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger btn-sm dltBtn"
                                            style="height:30px; width:30px;border-radius:50%"><i
                                            class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span style="float:right">{{$tasks->links()}}</span>

            @else
                <h6 class="text-center">No banners found!!! Please create banner</h6>
            @endif
        </div>
    </div>
@endsection
