@extends('layouts.app')

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary float-left">Task</h6>
            <a href="{{route('tasks.index')}}" class="btn btn-primary btn-sm float-right" data-toggle="tooltip" data-placement="bottom"
               title="Add User"><i class="fas fa-plus"></i>Close</a>
        </div>

    </div>
    <div class="card-body">
        <div class="table-responsive">
                <table class="table table-bordered" id="banner-dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Content</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$task->id}}</td>
                            <td>{{$task->name}}</td>
                            <td>{{$task->content}}</td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div>
@endsection
