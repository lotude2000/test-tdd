<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    public function getCreateTaskRoute() {
        return route('tasks.create');
    }
    public function getStoreTaskRoute() {
        return route('tasks.store');
    }
    /** @test  */
    public function authenticated_user_can_view_create_task_form()
    {
        $this->actingAs(User::factory()->make());
        $response = $this->get($this->getCreateTaskRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.create');
    }

    /** @test  */
    public function unauthenticated_user_can_not_view_create_task_form()
    {
        $response = $this->get($this->getCreateTaskRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_user_can_create_new_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getStoreTaskRoute(),$task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('tasks.index'));
        $this->assertDatabaseHas('tasks',$task);
    }

    /** @test  */
    public function authenticated_user_can_not_create_new_task_if_task_name_not_validate()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->post($this->getStoreTaskRoute(),$task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_user_can_not_create_new_task_if_task_name_and_content_not_validate()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->make([
            'name'=>'',
            'content'=>''
        ])->toArray();
        $response = $this->post($this->getStoreTaskRoute(),$task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','content']);
    }
}
