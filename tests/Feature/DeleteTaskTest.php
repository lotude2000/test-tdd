<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function getDeleteTaskRoute($id) {
        return route('tasks.destroy',$id);
    }
    /** @test  */
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('tasks',$task->toArray());
        $response->assertRedirect('tasks');
    }

    /** @test  */
    public function authenticated_user_can_not_delete_task_if_task_id_fail()
    {
        $this->actingAs(User::factory()->make());
        $taskId = -1;
        $response = $this->delete($this->getDeleteTaskRoute($taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
