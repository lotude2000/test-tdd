<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    public function getListTaskRoute() {
        return route('tasks.index');
    }
    /** @test  */
    public function authenticated_user_can_get_list_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_list_task()
    {
        $response = $this->get($this->getListTaskRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }
}
