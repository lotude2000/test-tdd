<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetTaskTest extends TestCase
{
    public function getTaskRoute($id) {
        return route('tasks.show',$id);
    }

    /** @test */
    public function authenticated_user_can_get_view_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get($this->getTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.view');
        $response->assertSee($task->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_view_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_user_can_not_get_view_task_if_task_id_fail()
    {
        $this->actingAs(User::factory()->make());
        $taskId = -1 ;
        $response = $this->get($this->getTaskRoute($taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
