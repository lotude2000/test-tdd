<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function getEditTaskRoute($id) {
        return route('tasks.edit',$id);
    }
    public function getUpdateTaskRoute($id) {
        return route('tasks.update',$id);
    }
    /** @test  */
    public function authenticated_user_can_view_update_task_form()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.edit');
        $response->assertSee($task->name);
    }

    /** @test  */
    public function unauthenticated_user_can_not_view_update_task_form()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test  */
    public function authenticated_user_can_not_view_update_task_form_if_task_id_fail()
    {
        $this->actingAs(User::factory()->make());
        $taskId = -1;
        $response = $this->get($this->getEditTaskRoute($taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function authenticated_user_can_update_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUPdate = [
          'name'=>$this->faker->name,
          'content'=>$this->faker->text
        ];
        $response = $this->patch($this->getUpdateTaskRoute($task->id),$dataUPdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('tasks');
        $this->assertDatabaseHas('tasks',[
            'name'=>$dataUPdate['name'],
            'content'=>$dataUPdate['content']
        ]);
    }

    /** @test  */
    public function authenticated_user_can_not_update_task_if_task_name_not_validate()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUPdate = [
            'name'=>'',
            'content'=>$this->faker->text
        ];
        $response = $this->patch($this->getUpdateTaskRoute($task->id),$dataUPdate);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test  */
    public function authenticated_user_can_not_update_task_if_task_name_and_content_not_validate()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUPdate = [
            'name'=>'',
            'content'=>''
        ];
        $response = $this->patch($this->getUpdateTaskRoute($task->id),$dataUPdate);
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
